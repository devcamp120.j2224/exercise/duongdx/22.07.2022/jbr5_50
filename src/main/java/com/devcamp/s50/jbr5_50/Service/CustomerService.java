package com.devcamp.s50.jbr5_50.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s50.jbr5_50.model.Customer;

@Service
public class CustomerService {
        Customer khach1 = new Customer(1, "Duong", 15);
        Customer khach2 = new Customer(2, "Linh", 20);
        Customer khach3 = new Customer(3, "Trang", 25);

        public ArrayList<Customer> gCustomers(){
            ArrayList<Customer> DanhSachKhach = new ArrayList<>();
            DanhSachKhach.add(khach1);
            DanhSachKhach.add(khach2);
            DanhSachKhach.add(khach3);
            return DanhSachKhach ;
            
        }
}
