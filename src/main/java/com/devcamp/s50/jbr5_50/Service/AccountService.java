package com.devcamp.s50.jbr5_50.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s50.jbr5_50.model.Account;

@Service
public class AccountService {
    @Autowired
    private CustomerService CustomerService ;
    Account nick1 = new Account(1, 12000);
    Account nick2 = new Account(2, 22000);
    Account nick3 = new Account(3, 32000);

    public ArrayList<Account> getNick(){
        ArrayList<Account> allNick = new ArrayList<>();
        nick1.setCustomer(CustomerService.khach1);
        nick2.setCustomer(CustomerService.khach2);
        nick3.setCustomer(CustomerService.khach3);

        allNick.add(nick1);
        allNick.add(nick2);
        allNick.add(nick3);

        return allNick ;
    }
}
