package com.devcamp.s50.jbr5_50.model;

public class Customer {
    private int id;
    private String name ;
    private int Discount ;
    public Customer(int id, String name, int discount) {
        this.id = id;
        this.name = name;
        Discount = discount;
    }
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public int getDiscount() {
        return Discount;
    }
    public void setDiscount(int discount) {
        Discount = discount;
    }
    @Override
    public String toString() {
        return "Customer [Discount=" + Discount + ", id=" + id + ", name=" + name + "]";
    }
    
}
