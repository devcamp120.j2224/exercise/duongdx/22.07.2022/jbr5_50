package com.devcamp.s50.jbr5_50.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s50.jbr5_50.Service.AccountService;
import com.devcamp.s50.jbr5_50.model.Account;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AccountController {
    @Autowired 
    private AccountService accountService;
    @GetMapping("/accounts")
    public ArrayList<Account> getAccountApi(){
        ArrayList<Account> accounts = accountService.getNick();
        return accounts ;
    }
}
